class AddContactFormToConfigurations < ActiveRecord::Migration
  def change
    add_column :configurations, :contact_page_id, :integer
  end
end
