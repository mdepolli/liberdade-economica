class AddDateToEdition < ActiveRecord::Migration
  def up
    add_column :editions, :date, :date
  end

  def down
    remove_column :editions, :date
  end
end
