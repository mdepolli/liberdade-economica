class CreateNodes < ActiveRecord::Migration
  def change
    create_table :nodes do |t|
      t.references :edition
      t.references :category

      t.timestamps
    end

    add_index :nodes, :edition_id
    add_index :nodes, :category_id
  end
end
