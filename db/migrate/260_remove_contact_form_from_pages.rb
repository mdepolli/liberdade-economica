class AddContactFormToPages < ActiveRecord::Migration
  def change
    add_column :pages, :contact_form, :boolean, :default => false
  end
end
class RemoveContactFormFromPages < ActiveRecord::Migration
  def change
    remove_column :pages, :contact_form
  end
end
