class CreateValuesSums < ActiveRecord::Migration
  def change
    create_table :values_sums do |t|
      t.references :category
      t.references :edition
      t.float :sum, default: 0

      t.timestamps
    end
  end
end
