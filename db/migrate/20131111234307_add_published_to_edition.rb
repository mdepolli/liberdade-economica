class AddPublishedToEdition < ActiveRecord::Migration
  def up
    add_column :editions, :published, :boolean, default: false
  end

  def down
    remove_column :editions, :boolean
  end
end
