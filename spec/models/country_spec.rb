# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Country do
  it { should_not have_valid(:name).when('', nil) }
  it { should have_valid(:name).when('Brazil', 'United States of America') }
end
