#encoding: utf-8

require 'spec_helper'

feature 'Category Management' do

  background do
    create :configuration
    user = create :user
    login(user.email, user.password)
    @category = create :category
  end

  before :each do
      visit '/admin/category'
  end

  context 'creation' do

    scenario 'should be able to create' do
      visit '/admin/category/new'
      fill_in 'Nome', with: 'Category 1'
      first(:button, 'Salvar').click
      page.should have_content 'Indicador criado(a) com sucesso.'
    end
  end

  context 'edit' do
    before :each do
      visit "/admin/category/#{@category.id}/edit"
    end

    scenario 'successfully' do
      fill_in 'Nome', with: 'Category 3'
      first(:button, 'Salvar').click
      page.should have_content 'Indicador atualizado(a) com sucesso.'
    end

    context 'unsuccessfully' do
      scenario 'empty name' do
        fill_in 'Nome', with: ''
        first(:button, 'Salvar').click
        page.should have_content('Nome não pode ser vazio.')
      end
    end
  end

end
