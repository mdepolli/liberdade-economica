# Read about factories at https://github.com/thoughtbot/factory_girl
FactoryGirl.define do
  factory :edition do
    sequence :name  do |n|
      "Edition #{n}"
    end

    date Date.today
    published true

    node_fake do
      c = create(:category).id
      "[[\"1.\", #{c}]]"
    end
  end
end
