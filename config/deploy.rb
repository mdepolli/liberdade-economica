require 'bundler/capistrano'
require 'capistrano/ext/multistage'

set :database, 'mysql'
# set :unicorn_workers, 1
set :use_ssl, false


set :webserver, :passenger
set :websocket_rails, false

set :www_redirect, false

# depends of yard gem. see doc recipe.
set :doc, false

set :paperclip_optimizer, false

# # backup stuff
# set :backup, true
# set :backup_on, :s3

# # set :backup_host, 'ci.algorich.com.br'
# # set :backup_port, '22'
# # set :backup_user, 'root'
# set :backup_time, '12:00am'

# set :backup_notification, false
# # if backup_notification is false, you can delete all these notification stuff
# set :backup_notification_on_success, false
# set :backup_notification_on_warning, false
# set :backup_notification_on_failure, false

# set :notification_mail_from,            'sender@email.com'
# set :notification_mail_to,              'receiver@email.com'
# set :notification_mail_address,         'smtp.gmail.com'
# set :notification_mail_port,            587
# set :notification_mail_domain,          'your.host.name'
# set :notification_mail_user_name,       'sender@email.com'
# set :notification_mail_password,        'my_password'
# set :notification_mail_authentication,  'plain'
# set :notification_mail_encryption,      :starttls
# # end of backup stuff

set :log_rotate, true
set :log_rotate_type, 'time' # can be either 'size' or 'time'
set :log_rotate_value, 'daily'
set :log_rotate_keep, 7
set :backup_logs, false
set :log_backup_keep, 14

# set :use_delayed_job, true


load 'config/recipes/base'
# load 'config/recipes/nginx'
# load 'config/recipes/unicorn' if webserver == :unicorn
load 'config/recipes/passenger' if webserver == :passenger
# load 'config/recipes/websocket_rails' if websocket_rails
# load "config/recipes/#{database}"
# load 'config/recipes/nodejs'
# load 'config/recipes/rbenv'
load 'config/recipes/check'
# load 'config/recipes/delayed_job' if use_delayed_job
# load 'config/recipes/monit'
# load 'config/recipes/ufw'
# load 'config/recipes/fail2ban'
# load 'config/recipes/supervisord' if webserver == :unicorn
# load 'config/recipes/backup' if backup
# load 'config/recipes/log_rotate' if log_rotate
# load 'config/recipes/project_dependencies'
# load 'config/recipes/doc' if doc
# load 'config/recipes/info' # this should be the last recipe to be loaded

set :stages, %w(production staging)

set :application, 'liberdade_economica'

set :user, 'deploy'
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, 'git'
set :repository, "git@bitbucket.org:mdepolli/liberdade-economica.git"

set :maintenance_template_path, File.expand_path('../recipes/templates/maintenance.html.erb', __FILE__)

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

before "deploy:assets:precompile", "config_symlink"
after 'deploy', 'deploy:cleanup' # keep only the last 5 releases

task :config_symlink do
  run "ln -nfs #{deploy_to}/shared/config/database.yml #{release_path}/config/database.yml"
end
