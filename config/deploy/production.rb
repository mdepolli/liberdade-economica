set :rails_env, 'production'
set :server_name, '104.236.106.255'
server server_name, :web, :app, :db, primary: true
set :branch, 'master'
