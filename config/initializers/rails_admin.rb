# -*- encoding : utf-8 -*-

RailsAdmin.config do |config|
  config.main_app_name = [ 'Liberdade Econômica', 'Administração' ]

  config.current_user_method { current_user } #auto-generated
  config.yell_for_non_accessible_fields = false
  config.authorize_with :cancan

  config.actions do
    # root actions
    dashboard                     # mandatory
    # collection actions
    index                         # mandatory
    new
    export
    history_index
    bulk_delete
    # member actions
    show
    edit
    delete

    history_show
    show_in_app

    # Add the nestable action for each model
    # nestable do
    #   visible do
    #     %w(Category TreelessCategory).include? bindings[:abstract_model].model_name
    #   end
    # end
  end

###
### Humam freedom data
###

  config.model Value do
    navigation_label 'Índice'
    weight 1

    list do
      field :edition
      field :country
      field :total
    end

    edit do
      field :edition
      field :country
      field :total

      field :values do
        partial :values
      end
    end
  end

  config.model Edition do
    navigation_label 'Índice'
    weight 2

    list do
      field :name
      field :date do
        date_format :default
      end
      field :published
    end

    edit do
      field :name
      field :date do
        date_format :default
      end

      field :node_fake do
        partial :node_fake
      end

      field :published
    end
  end

  config.model Category do
    navigation_label 'Índice'
    weight 3

    list do
      field :name
    end

    edit do
      field :name
    end
  end

  config.model Country do
    navigation_label 'Índice'
    weight 4

    list do
      field :name
      field :code
    end

    edit do
      field :name
      field :code
      field :description
      field :labels
    end
  end

  config.model Page do
    navigation_label 'Páginas'
    weight 5

    list do
      field :title
      field :published
    end

    edit do
      field :title
      field(:content) { ckeditor true }
      field :published
    end
  end

  config.model Configuration do
    edit do
      field :freedom_val do
        help 'Índice mundial de liberdade econômica'
      end

      group :social do
        label 'Redes Sociais'
        active false
        field :twitter do
          help 'http://twitter.com/LiberdadeEconomica'
        end
        field :facebook do
          help 'http://facebook.com/LiberdadeEconomica'
        end
        field :facebook_app_id do
          help 'Necessário para o funcionamento correto do compartilhamento'
        end
      end

      group :contact do
        label 'Contato'
        active false
        field :email do
          help 'E-mail que receberá as mensagens de contato'
        end
        field :contact_page do
          help 'Página que terá o formulário de contato'
        end
      end

      group :search do
        label 'Informações para buscadodes / Google Analytics'
        active false
        field :keywords
        field :description
        field :google_analytics
      end

      group :update do
        label 'Atualizar dados complementares'
        active false

        field :hidden_field do
          partial :hidden_field
          help 'Esta operação pode demorar alguns minutos. No decorrer de sua
            execução a aplicação ficará fora de funcionamento.'
        end
      end
    end
  end

  config.model Label do
    visible false
  end

  config.model Node do
    visible false
  end


  config.model Proponent do
    visible false
  end

  config.model Proposal do
    visible false
  end

  config.model ValuesLabelsSum do
    visible false
  end

  config.model ValuesSum do
    visible false
  end

end
