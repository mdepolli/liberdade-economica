# -*- encoding : utf-8 -*-

LiberdadeEconomica::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  match '/delayed_job' => DelayedJobWeb, anchor: false

  # 1. user routes
  devise_for :users

  # 2. contact routes
  get '/contact' => 'site#contact', as: :contact
  post '/contact' => 'site#send_contact', as: :send_contact

  get '/ranking/:edition' => 'site#ranking', as: :ranking
  get '/pais/:name' => 'site#country', as: :country
  get '/dados' => 'site#data', as: :data
  get '/grafico/:category(/:country_1(/:country_2))' => 'site#graph', as: :graph
  get '/grafico_geral/:category' => 'site#general_graph', as: :general_graph
  get '/comparar/:name1/:name2' => 'site#comparate', as: :comparate
  get '/change_map/:id' => 'site#change_map', as: :change_map

  get '/update_vals' => 'site#update_vals', as: :update_vals

  get '/:page_title' => 'site#page', as: :page

  root :to => 'site#index'
end
