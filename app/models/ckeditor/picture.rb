# -*- encoding : utf-8 -*-

class Ckeditor::Picture < Ckeditor::Asset

  has_attached_file :data,
    :url  => '/system/ckeditor_assets/pictures/:id/:style_:basename.:extension',
    :path => ':rails_root/public/system/ckeditor_assets/pictures/:id/:style_:basename.:extension',
    :styles => { :content => '800>', :thumb => '118x100#' }

  validates_attachment_size :data, :less_than => 2.megabytes
  validates_attachment_presence :data

  attr_accessible :data

  def url_content
    url(:content)
  end

  rails_admin do
    label 'Imagem'
    label_plural 'Imagens'
    navigation_label 'Arquivos adicionados através do editor'
    weight 1

    edit do
      field(:data) { label 'Imagem' }
    end

    list do
      field(:data) { label 'Imagem' }
      field(:created_at) { label 'Criado em' }
      field(:updated_at) { label 'Atualizado em' }
    end
  end

end
