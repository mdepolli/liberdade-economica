class Category < ActiveRecord::Base
  attr_accessible :name, :nodes, :values, :editions

  has_many :nodes, dependent: :destroy

  has_many :values_sum
  has_many :editions, through: :values_sum

  validates :name, presence: true

  def editions
    edts = []

    Edition.all.each do |e|
      edts << e if e.categories.include? self
    end

    return edts
  end

end
