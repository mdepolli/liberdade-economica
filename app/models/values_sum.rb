class ValuesSum < ActiveRecord::Base
  attr_accessible :category, :edition, :sum, :category_id, :edition_id

  belongs_to :category
  belongs_to :edition
end
